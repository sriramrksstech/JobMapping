import os
import base64

#from Bio.SeqUtils import seq3
#from Bio.Alphabet import generic_dna, generic_rna
#from Bio.Seq import Seq
#from Bio.Data.CodonTable import TranslationError
from dash.dependencies import Input, Output, State
import dash_html_components as html
import dash_core_components as dcc
from urllib.parse import quote as urlquote
import pandas as pd
import base64
import datetime
import io


#from dash_bio_utils import protein_reader as pr

#import dash_bio

try:
    from layout_helper import run_standalone_app
except ModuleNotFoundError:
    from layout_helper import run_standalone_app




DATAPATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
print(os.getcwd())
proteinFolder = 'proteins'
sequence = '--'

initialCov = [
    {'start': 26, 'end': 29, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 33, 'end': 43, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 44, 'end': 46, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 48, 'end': 50, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 56, 'end': 58, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 59, 'end': 66, 'color': 'rgb(0,0,200)',
     'bgcolor': 'rgb(200,200,0)', 'tooltip': 'Turn', 'underscore': False},
    {'start': 74, 'end': 76, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 79, 'end': 81, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 84, 'end': 86, 'color': 'rgb(0,0,200)',
     'bgcolor': 'rgb(200,200,0)', 'tooltip': 'Turn', 'underscore': False},
    {'start': 91, 'end': 97, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 98, 'end': 101, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 102, 'end': 106, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 107, 'end': 109, 'color': 'rgb(0,0,200)',
     'bgcolor': 'rgb(200,200,0)', 'tooltip': 'Turn', 'underscore': False}
]


def header_colors():
    return {
        'bg_color': '#fff',
        'font_color': 'black'
    }


def description():
    return 'Display protein and nucleotide sequences with \
    coverages, selection information, and search.'


def layout():

    return html.Div(id='seq-view-body', className='app-body', children=[
        html.Div(
            id='seq-view-container',
            children=[
                html.Div(id='seq-view-info-container', children=html.Div(
                    id='seq-view-info',
                    children=[
                        html.Div(id='seq-view-info-desc',
                                 children=[
                                     html.Span(
                                         "Description",
                                         className='seq-view-info-element-title'
                                     ),
                                     html.Div(
                                         id='desc-info',
                                         children=[]
                                     )
                                 ]),
						html.Div(id='text-info-desc',
                                 children=[
                                     html.Span(
                                         "Text",
                                         className='seq-view-info-element-title'
                                     ),
                                     html.Div(
                                         id='text-info',
                                         children=[]
                                     )
                                 ]),	 

                        html.Br(),

                        html.Div(id='seq-view-info-aa-comp',
                                 children=[
                                     html.Span(
                                         "Amino acid composition",
                                         className='seq-view-info-element-title'
                                     ),
                                     html.Div(
                                         id='test-selection'
                                     )
                                 ]),

                        html.Br(),

                        html.Div(id='seq-view-info-coverage-clicked',
                                 children=[
                                     html.Span(
                                         "Coverage entry clicked",
                                         className='seq-view-info-element-title'
                                     ),
                                     html.Div(
                                         id='test-coverage-clicked'
                                     )
                                 ]),

                        html.Br(),

                        html.Div(id='seq-view-info-mouse-selection',
                                 children=[
                                     html.Span(
                                         "Mouse selection",
                                         className='seq-view-info-element-title'
                                     ),
                                     html.Div(
                                         id='test-mouse-selection'
                                     )
                                 ]),

                        html.Br(),

                        html.Div(id='seq-view-info-subpart-sel', children=[
                            html.Span(
                                "Subpart selected",
                                className='seq-view-info-element-title'
                            ),
                            html.Div(
                                id='test-subpart-selection'
                            )
                        ])
                    ]
                )),
				html.Div(
                    id='seq-view-component-container-1',
                    children=[
						dcc.Graph(
							id='example-graph1',
							figure={
								'data': [
									{'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
									{'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
								],
								'layout': {
									'title': 'Dash Data Visualization'
								}
							}
						)
					]
                ),
				 html.Div(id='hidden-div', style={'display':'none'}),
				
            ]
        ),
        html.Div(id='seq-view-control-tabs', className='control-tabs', children=[
			html.Div(
            id='seq-view-containertest',
            children=[
            dcc.Tabs(id='seq-view-tabs', value='what-is', children=[
                dcc.Tab(
                    label='About',
                    value='what-is',
                    children=html.Div(className='control-tab', children=[
                        html.H4(className='what-is', children='What is Sequence Viewer?'),
                        html.P('Sequence Viewer is a component that allows you '
                               'to display genomic and proteomic sequences. In '
                               'this app, you can choose to view one of the preloaded '
                               'data sets or upload your own FASTA file in the "Data" '
                               'tab. For FASTA files with multiple entries, the entry '
                               'to display in the component can be selected in the '
                               '"Sequence" tab.'),
                        html.P('In the "Sequence" tab, you can also select a region '
                               'of the sequence to highlight and view its amino '
                               'acid composition in the box under the component. '),
                        html.P('You can additionally create a sequence coverage '
                               '(i.e., a collection of subsequences to highlight '
                               'and annotate). These subsequences can be extracted '
                               'from your mouse selection, or from the results of the '
                               'search that is shown in the component. Upon clicking on '
                               'a coverage entry, you will be able to see the '
                               'annotation that you have provided.'),
                    ])
                ),
                dcc.Tab(
                    label='Data',
                    value='data',
                    children=html.Div(className='control-tab', children=[
						html.Div(
                            id='seq-view-fasta-upload',
                            children=[
							 html.Label(["Step 1: Upload Resume File ",html.Br(),
                                dcc.Upload(
                                    id='upload-fasta-data',
                                    className='control-upload',
                                    children=html.Div([
                                        "Drag and drop or click to upload a \
                                        Resume File."
                                    ]),multiple=True
                                ),
                            ])
							]
                        ),
						
                        html.Div(className='app-controls-block', children=[
							html.Div(
							html.Label(["Step 2: Select a Job Role ",html.Br(),
                            dcc.Dropdown(
                                className='app-dropdown',
                                id='preloaded-sequences',
                                options=[
                                    {
                                        'label': 'Data Scientist',
										'value': 'Data Scientist'
                                    },
                                    {
                                        'label': 'Machine Learning Engineer',
										'value': 'Machine Learning Engineer',
                                    }
                                ],placeholder="Select a Job Role",
                             
                            )
							]))
                        ]),
						html.Div(className='app-controls-block', children=[
                            html.Div(
                                html.Label(["Step 3: Enter Job Description(Optional) ",html.Br(),
                            dcc.Textarea(
                                className='app-dropdown',
                                id='preloaded-sequences-textarea',
								value='Please Copy-Paste Job Description Here',
								style={'width': '90%', 'height': 200},
                            ),
							html.Div(
                                    id='textarea-example-output',
                                    className='app-controls-name'
                                )
                        ])
						)
						]),
                     
                    ])
                ),
                dcc.Tab(
                    label='Sequence',
                    value='sequence',
                    children=html.Div(className='control-tab', children=[
                        html.Div(
                            id='seq-view-entry-dropdown-container',
                            className='app-controls-block',
                            children=[
                                html.Div(
                                    "View entry:",
                                    className='app-controls-name'
                                ),
                                dcc.Dropdown(
                                    className='app-dropdown',
                                    id='fasta-entry-dropdown',
                                    options=[
                                        {'label': 1, 'value': 0}
                                    ],
                                    value=0
                                ),
                                html.Div(
                                    id='seq-view-number-entries',
                                    className='app-controls-desc'
                                )
                            ]
                        ),
                        html.Br(),
						html.Div(
                            id='seq-view-fasta1-upload',
                            children=[
                                dcc.Upload(
                                    id='upload1-fasta-data',
                                    className='control1-upload',
                                    children=html.Div([
                                        "Drag and drop or click to upload a \
                                        file."
                                    ]),
                                ),
                            ]
                        ),
						html.Br(),
                        html.Div(
                            id='seq-view-sel-or-cov-container',
                            children=[
                                html.Div(
                                    "Selection or coverage:",
                                    className='app-controls-name'
                                ),
                                dcc.RadioItems(
                                    id='selection-or-coverage',
                                    options=[
                                        {
                                            'label': 'selection',
                                            'value': 'sel'
                                        },
                                        {
                                            'label': 'coverage',
                                            'value': 'cov'
                                        }
                                    ],
                                    value='sel'
                                )
                            ]
                        ),


                        html.Hr(),

                        html.Div(id='cov-options', children=[
                            html.Div(className='app-controls-block', children=[
                                html.Div(
                                    "Add coverage from selection by:",
                                    className='app-controls-name'
                                ),
                                dcc.RadioItems(
                                    id='mouse-sel-or-subpart-sel',
                                    options=[
                                        {'label': 'mouse',
                                         'value': 'mouse'},
                                        {'label': 'search',
                                         'value': 'subpart'}
                                    ],
                                    value='mouse'
                                ),
                            ]),

                            html.Div(className='app-controls-block', children=[
                                html.Div(
                                    "Text color:",
                                    className='app-controls-name'
                                ),
                                dcc.Input(
                                    id='coverage-color',
                                    type='text',
                                    value='rgb(255, 0, 0)'
                                )
                            ]),
                            html.Div(className='app-controls-block', children=[
                                html.Div(
                                    "Background color:",
                                    className='app-controls-name'
                                ),
                                dcc.Input(
                                    id='coverage-bg-color',
                                    type='text',
                                    value='rgb(0, 0, 255)'
                                ),
                            ]),
                            html.Div(className='app-controls-block', children=[
                                html.Div(
                                    "Tooltip:",
                                    className='app-controls-name'
                                ),
                                dcc.Input(
                                    id='coverage-tooltip',
                                    type='text',
                                    value='',
                                    placeholder='hover text'
                                ),
                            ]),
                            html.Div(className='app-controls-block', children=[
                                html.Div(
                                    "Underscore text: ",
                                    className='app-controls-name'
                                ),
                                dcc.Checklist(
                                    id='coverage-underscore',
                                    options=[
                                        {'label': '',
                                         'value': 'underscore'}
                                    ],
                                    value=[]
                                )
                            ]),
                            html.Div(className='app-controls-block', children=[
                                html.Button(
                                    id='coverage-submit',
                                    children='Submit'
                                ),
                                html.Button(
                                    id='coverage-reset',
                                    children='Reset'
                                )
                            ])
                        ]),

                        html.Div(id='seq-view-sel-slider-container', children=[
                            html.Div(className='app-controls-block', children=[
                                html.Div(
                                    className='app-controls-name',
                                    children="Selection region:"
                                ),
                                dcc.RadioItems(
                                    id='sel-slider-or-input',
                                    options=[
                                        {'label': 'slider', 'value': 'slider'},
                                        {'label': 'input', 'value': 'input'}
                                    ],
                                    value='slider'
                                )
                            ]),
                            html.Div(className='app-controls-block', children=[
                                dcc.RangeSlider(
                                    id='sel-slider',
                                    min=0,
                                    max=0,
                                    step=1,
                                    value=[0, 0]
                                )
                            ]),
                            html.Div(className='app-controls-block', children=[
                                # optional numeric input for longer sequences
                                html.Div(
                                    id='sel-region-inputs',
                                    children=[
                                        "From: ",
                                        dcc.Input(
                                            id='sel-region-low',
                                            type='number',
                                            min=0,
                                            max=0,
                                            placeholder="low"
                                        ),
                                        "To: ",
                                        dcc.Input(
                                            id='sel-region-high',
                                            type='number',
                                            min=0,
                                            max=0,
                                            placeholder="high"
                                        ),
                                    ],
                                    style={'display': 'none'}
                                )
                            ]),

                            html.Div(
                                id='seq-view-dna-or-protein-container',
                                children=[
                                    html.Div(className='app-controls-block', children=[
                                        html.Div(
                                            className='app-controls-name',
                                            children="Translate selection from:"
                                        ),
                                        dcc.Dropdown(
                                            id='translation-alphabet',
                                            options=[
                                                {'label': 'DNA',
                                                 'value': 'dna'},
                                                {'label': 'RNA',
                                                 'value': 'rna'}
                                            ],
                                            value=None
                                        )
                                    ])
                                ]
                            ),

                            html.Div(
                                className='app-controls-name',
                                children="Selection highlight color:"
                            ),
                            dcc.Dropdown(
                                className='app-dropdown',
                                id='sel-color',
                                options=[
                                    {'label': 'violet', 'value': 'violet'},
                                    {'label': 'indigo', 'value': 'indigo'},
                                    {'label': 'blue', 'value': 'blue'},
                                    {'label': 'green', 'value': 'green'},
                                    {'label': 'yellow', 'value': 'yellow'},
                                    {'label': 'orange', 'value': 'orange'},
                                    {'label': 'red', 'value': 'red'}
                                ],
                                value='indigo'
                            )
                        ])
                    ])
                ),
				dcc.Tab(
                    label='extra',
                    value='hello',
                    children=html.Div(children=[
						html.H1(children='Hello Dash'),

						html.Div(children='''
							Dash: A web application framework for Python.
						'''),

						dcc.Graph(
							id='example-graph',
							figure={
								'data': [
									{'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
									{'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
								],
								'layout': {
									'title': 'Dash Data Visualization'
								}
							}
						)
					])
                )
            ]),
            
			]),
			
        ]),
        dcc.Store(
            id='coverage-storage',
            data=initialCov
        ),
        dcc.Store(
            id='clear-coverage',
            data=0
        ),
        dcc.Store(
            id='current-sequence',
            data=0
        )
    ])


def callbacks(_app):

# upload or preloaded
    
		
  
    @_app.callback(
        Output('text-info', 'children'),
        [Input('preloaded-sequences-textarea', 'value')]
    )
    def update_output(value):
        return 'You have entered: \n{}'.format(value)
	
    
    def parse_contents(contents, filename, date):
       content_type, content_string = contents.split(',')
       decoded = base64.b64decode(content_string)
      
       return decoded.decode('utf-8')
		
	
    @_app.callback(Output('desc-info', 'children'),
              [Input('upload-fasta-data', 'contents')],
              [State('upload-fasta-data', 'filename'),
               State('upload-fasta-data', 'last_modified')])
    def update_output(list_of_contents, list_of_names, list_of_dates):
        
        if list_of_contents is not None:
            children = [
                parse_contents(c, n, d) for c, n, d in
                zip(list_of_contents, list_of_names, list_of_dates)]
			
            return children

    UPLOAD_DIRECTORY = "/root/test/tejaswi/dash-bio/tests/dashbio_demos/dash-sequence-viewer/UploadFiles"


    if not os.path.exists(UPLOAD_DIRECTORY):
        os.makedirs(UPLOAD_DIRECTORY)
		
    def download(path):
        """Serve a file from the upload directory."""
        return send_from_directory(UPLOAD_DIRECTORY, path, as_attachment=True)
	
    def save_file(name, content):
        """Decode and store a file uploaded with Plotly Dash."""
        data = content.encode("utf8").split(b";base64,")[1]
        with open(os.path.join(UPLOAD_DIRECTORY, name), "wb") as fp:
            fp.write(base64.decodebytes(data))


    def uploaded_files():
        """List the files in the upload directory."""
        files = []
        for filename in os.listdir(UPLOAD_DIRECTORY):
            path = os.path.join(UPLOAD_DIRECTORY, filename)
            if os.path.isfile(path):
                files.append(filename)
        return files


    def file_download_link(filename):
        """Create a Plotly Dash 'A' element that downloads a file from the app."""
        location = "/download/{}".format(urlquote(filename))
        return html.A(filename, href=location)


    @_app.callback(
        Output("hidden-div", "children"),
        [Input("upload-fasta-data", "filename"), Input("upload-fasta-data", "contents")],
)


    def update_output(uploaded_filenames, uploaded_file_contents):
            """Save uploaded files and regenerate the file list."""

            if uploaded_filenames is not None and uploaded_file_contents is not None:
                   for name, data in zip(uploaded_filenames, uploaded_file_contents):
                        save_file(name, data)

            files = uploaded_files()
            if len(files) == 0:
                return [html.Li("No files yet!")]
            else:
                return [html.Li(file_download_link(filename)) for filename in files]
    


# only declare app/server if the file is being run directly
if 'DEMO_STANDALONE' not in os.environ:
    app = run_standalone_app(layout, callbacks, header_colors, __file__)
    server = app.server

if __name__ == '__main__':
    app.run_server(debug=True, port=8050)

