#Libraries
import json
import PyPDF2
import nltk
from nltk import word_tokenize
from nltk.tokenize import WordPunctTokenizer
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import string
import docx2txt
import os
import glob
import pandas as pd


AIML_PATH = os.path.join(
                os.path.dirname(
                    os.path.abspath(
                        os.getcwd())),'dash-sequence-viewer\\AIML\\*')

UPLOAD_DIR_PATH = os.path.join(
            os.path.dirname(
                os.path.abspath(
                    os.getcwd())),'dash-sequence-viewer\\upload_dir\\*')


#Functions
def process_pdf(filepath):
    try:
        fileReader = PyPDF2.PdfFileReader(open(filepath,'rb'))
        countpage = fileReader.getNumPages()
        count = 0
        text = ""
        while count < countpage:
            page_Obj = fileReader.getPage(count)
            count +=1
            txt = page_Obj.extractText()
            text = text + txt
        return 1,text
    except:
        return 0,""

def process_word(filepath):
    try:
        text = docx2txt.process(filepath)
        return 1,text
    except:
        return 0,""

def process_text(filepath):
    try:
        text = open(filepath).read()
        return 1,text
    except:
        return 0,""

def preprocessing(resume_txt):
    resume_txt = resume_txt.lower()
    text_clean = "".join(e if e.isalnum() else " " for e in resume_txt)
    lemmatizer = WordNetLemmatizer()
    wordpunct_tokenize = WordPunctTokenizer().tokenize
    all_tokens = wordpunct_tokenize(text_clean)
    tokens = [lemmatizer.lemmatize(token) for token in all_tokens if token not in stopwords.words("english") and token[0].isalpha()]
    return tokens

def keyword_count(tokens):
    bigrams = nltk.bigrams(tokens)
    unigram_freq = nltk.FreqDist(tokens)
    bigram_freq = nltk.FreqDist(bigrams)
    counts = {}
    for key,value in unigram_freq.items():
        if key in counts:
            counts[key] += value
        else:
            counts[key] = value
    for key_tup,value in bigram_freq.items():
        key = ' '.join(key_tup)
        if key in counts:
            counts[key] += value
        else:
            counts[key] = value
    return counts

def get_resume_counts(filepath):
    filename = filepath.rsplit('/', 1)[-1]
    fileExtension = filepath.rsplit('.', 1)[-1]
    text = ""
    status=0
    if(fileExtension.lower() == "pdf"):
        status,text = process_pdf(filepath)
    elif(fileExtension.lower() == "docx"):
        status,text = process_word(filepath)
    elif(fileExtension.lower() == "txt"):
        status,text = process_text(filepath)
    if(status==0):
        print("Input not found")
        return 0,[]
    else:
        tokens = preprocessing(text)
        counts = keyword_count(tokens)
        return 1,counts

def process_jd(filepath):
    text = open(filepath).read()
    tokens = preprocessing(text)
    print(len(tokens))
    counts = keyword_count(tokens)
    return counts

def best_fit(filepath):
    status, resume_counts = get_resume_counts(filepath)
    role_name,primary_metadata,secondary_metadata = process_metadata()
    final_count_match = {}
    final_count = 0
    length = len(primary_metadata)
    for i in range(length):
        primary_count, secondary_count, primary_dict, secondary_dict = metadata_match(primary_metadata[i],secondary_metadata[i],resume_counts)
        final_count = 2*primary_count + secondary_count
        final_count_match[role_name[i]] = final_count
    return final_count_match, resume_counts

#Doing Metadata Match that return counts for primary and secondary skills
def metadata_match(primary_metadata,secondary_metadata,skills_resume):
    primary_count = secondary_count = 0
    primary_dict = {}
    secondary_dict = {}
    for key in primary_metadata:
        if key in skills_resume:
            primary_dict[key] = skills_resume[key]
            primary_count += skills_resume[key]
    for key in secondary_metadata:
        if key in skills_resume:
            secondary_dict[key] = skills_resume[key]
            secondary_count += skills_resume[key]
    return primary_count, secondary_count, primary_dict, secondary_dict

def all_job_roles():
    rolefile = max(
        glob.glob(AIML_PATH,key = os.path.getmtime)
    file = open(rolefile)
    analytics = json.load(file)
    role_name = []
    for role_obj in analytics['Roles']:
        role_name.append(role_obj['Name'])
    return role_name

############# Handling functions for resume,job role and job descriptions Comparisons ######################
def job_role_handler(role_name,filename):
    primary_cnt={}
    secondary_cnt={}
    rolefile = open(max(
        glob.glob(AIML_PATH),key = os.path.getmtime))
    status,resume_counts = get_resume_counts(filename)
    print(rolefile)
    analytics = json.load(rolefile)
    role_obj = [role for role in analytics['Roles'] if role['Name'] == role_name]
    print("role_obj is" )
    print(role_obj)
    primary_skills = [role.lower() for role in role_obj[0]['Skills']['Primary']]
    secondary_skills = [role.lower() for role in role_obj[0]['Skills']['Secondary']]
    for i in resume_counts.keys():
        if i in primary_skills:
            if i not in primary_cnt.keys():
                primary_cnt[i]=0        
            primary_cnt[i] +=resume_counts[i]
        if i in secondary_skills:
            if i not in secondary_cnt.keys():
                secondary_cnt[i]=0
            secondary_cnt[i] +=resume_counts[i]
    num = sum(primary_cnt.values())+sum(secondary_cnt.values())
    den = (len(primary_skills)+len(secondary_skills))
    matching_percent = num/den
    return [primary_cnt,secondary_cnt,matching_percent]

def jd_handler(text,filename):
    tokens = preprocessing(text)
    jd_counts = keyword_count(tokens)
    status,resume_counts = get_resume_counts(filename)
    match_counts={}
    if status == 1:
        for i in resume_counts.keys():
            if i in jd_counts.keys():
                if i not in match_counts.keys():
                    match_counts[i]=0        
                match_counts[i] +=resume_counts[i]
    
    matching_percent = sum(match_counts.values())/sum(jd_counts.values())
    print("matching_percent jd is")
    print(matching_percent,str(round(matching_percent*100,2))+"%")
    match = pd.DataFrame.from_dict( \
        match_counts,orient='index',columns=['count']).sort_values( \
            by='count',ascending=False)
    return match,matching_percent    

def get_match_percent(role,key='jobrole'):
    ### role keyword accepts either a job role or a Jd description ###
    files= glob.glob(UPLOAD_DIR_PATH)
    print(role)
    filename=max(files,key = os.path.getmtime)
    if key == 'jobrole':
        return job_role_handler(role,filename)
    elif key == 'jd':
        match,matching_percent= jd_handler(role,filename)
        return match,str(round(matching_percent*100,2))+"%"


def del_uploaded_files():
    path=UPLOAD_DIR_PATH
    files= glob.glob(path)
    for f in files:
        os.remove(f)
    return "all files deleted in "+str(path)


def resume_lister(role,key='jobRole'):
    path=UPLOAD_DIR_PATH
    files= glob.glob(path)
    resume_list=[]

    for f in files:
        if key == 'jobRole':
            ps,ss,matching_percent = job_role_handler(role,f)
            resume_list.append({'filename': f,'Percent_match':matching_percent})
        elif key == 'jd':
            match_df,matching_percent = jd_handler(role,f)
            resume_list.append({'filename': f,'Percent_match':matching_percent})
            print("got here")
    resume_df =pd.DataFrame(resume_list).sort_values(by='Percent_match',ascending=False)
    return resume_df[['filename','Percent_match']]


def process_metadata():
    rolefile = max(
        glob.glob(AIML_PATH),key = os.path.getmtime)
    file = open(rolefile)
    analytics = json.load(file)
    lemmatizer = WordNetLemmatizer()
    primary_skills = []
    secondary_skills = []
    role_name = []
    for role_obj in analytics['Roles']:
        # role_obj = [role for role in analytics['Roles'] if role['Name'] == "Data Scientist"]
#    primary_skills = [role.lower() for role in role_obj[0]['Skills']['Primary']]
#    secondary_skills = [role.lower() for role in role[0]['Skills']['Secondary']]
        role_name.append(role_obj['Name'])
        primary_skill = [lemmatizer.lemmatize(role.lower()) for role in role_obj['Skills']['Primary']]
        secondary_skill = [lemmatizer.lemmatize(role.lower()) for role in role_obj['Skills']['Secondary']]
        primary_skills.append(primary_skill)
        secondary_skills.append(secondary_skill)
    return role_name,primary_skills, secondary_skills


