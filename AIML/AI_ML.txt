{
	"Title": "AI/ML",
	"Roles": [
		{
			"Name": "Data Scientist",
			"Skills": {
				"Primary": [
					"R",
					"Python",
					"Hive",
					"Hypothesis Testing",
					"Regression",
					"Classification",
					"Supervised",
					"Unsupervised",
					"SVM",
					"USVM",
					"Reinforcement ML",
					"NLP",
					"Natural Language Processing",
					"Phd in Statistics",
					"Phd in Mathematics",
					"Numpy",
					"Pandas",
					"Matplotlib",
					"PyTorch",
					"Scikit Learn",
					"sklearn",
					"TensorFlow",
					"Knime",
					"Keras",
					"SQL"
				],
				"Secondary": [
					"Spark",
					"Scala",
					"Java",
					"Pig",
					"Impala",
					"Research",
					"Computer Science",
					"CPLEX",
					"AMPL",
					"Hadoop",
					"HBase",
					"Cassandra",
					"MongoDB",
					"Neural Network"
				]
			}
		},
		{
			"Name": "Machine Learning Engineer",
			"Skills": {
				"Primary": [
					"Learning To Rank",
					"Machine learning",
					"NLP",
					"Natural Language Processing",
					"PyTorch",
					"Dataproc",
					"TensorFlow",
					"CUDA",
					"SQL",
					"Pandas",
					"Keras",
					"scikit",
					"Concourse",
					"graph analyzing",
					"processing tools",
					"Matplotlib",
					"neural networks",
					"deep learning"
				],
				"Secondary": [
					"Go",
					"Cloud-Native Applications",
					"Spark",
					"Hadoop",
					"BigQuery",
					"Hive",
					"RankLib",
					"Kubernetes",
					"Dataproc",
					"Docker",
					"Cloud Foundry",
					"Airflow",
					"Beam",
					"SPARQL",
					"RDF",
					"Solr",
					"Lucene",
					"DataFrame"
				]
			}
		}
	]
}