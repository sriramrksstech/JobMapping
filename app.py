import os
import base64

#from Bio.SeqUtils import seq3
#from Bio.Alphabet import generic_dna, generic_rna
#from Bio.Seq import Seq
#from Bio.Data.CodonTable import TranslationError
from dash.dependencies import Input, Output, State
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
from urllib.parse import quote as urlquote
import pandas as pd
import base64
import datetime
import dash.dependencies as dd
from io import BytesIO


import pandas as pd
#from wordcloud import WordCloud
import base64
import io
import NLP
import dash_table
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
# from chart_studio.plotly import iplot
# from plotly.offline import init_notebook_mode, iplot
# init_notebook_mode(connected=True)
import plotly.graph_objs as go

import matplotlib.pyplot as plt


#from dash_bio_utils import protein_reader as pr

#import dash_bio

try:
    from layout_helper import run_standalone_app
except ModuleNotFoundError:
    from layout_helper import run_standalone_app




DATAPATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
print(os.getcwd())
proteinFolder = 'proteins'
sequence = '--'
fig =go.Figure(go.Sunburst(
    labels=["Eve", "Cain", "Seth", "Enos", "Noam", "Abel", "Awan", "Enoch", "Azura"],
    parents=["", "Eve", "Eve", "Seth", "Seth", "Eve", "Eve", "Awan", "Eve" ],
    values=[10, 14, 12, 10, 2, 6, 6, 4, 4],
))

dfm = pd.DataFrame({'word': ['python','excel','machine_learning','tableau','sql'], 'freq': [1,3,9,10,11]})

############### LAYOUT UTILITY FUNCTIONS #####################
def tablebuilder(df,val):
    col_list=[]
    for col in df.columns:
        col_list.append({"name":col,"id":col})
    return dash_table.DataTable(
                        id='table-info',
                        columns = col_list,
                        data=df.to_dict('records'),
                        )

def getmatch_layout(df1,df2,val):
   return html.Div(
        id='seq-view-info',
        children=[html.Div(id='seq-view-info-desc',children=[
        html.Span(
                    "Match - Resume vs jobRole",
                    className='seq-view-info-element-title'
                ),html.Div(
                    id='desc-info',
                    children=[tablebuilder(df1,val),dbc.Button(
                        children =str(val), id="example-button", className="mr-2")
                    ]
                ),html.Span(
                    "Match - Resume vs jobDescription",
                    className='seq-view-info-element-title'
                ),html.Div(
                    id='jd-info',
                    children=[tablebuilder(df2,val),dbc.Button(
                        children =str(val), id="example-button", className="mr-2")
                    ]
                )    
         ] ),
            html.Div(id='text-info-desc',
                        children=[
                            html.Span(
                                "Text",
                                className='seq-view-info-element-title'
                            ),
                            html.Div(
                                id='text-info',
                                children=[]
                            )
                        ]),
            
        ]
    )
    



initialCov = [
    {'start': 26, 'end': 29, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 33, 'end': 43, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 44, 'end': 46, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 48, 'end': 50, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 56, 'end': 58, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 59, 'end': 66, 'color': 'rgb(0,0,200)',
     'bgcolor': 'rgb(200,200,0)', 'tooltip': 'Turn', 'underscore': False},
    {'start': 74, 'end': 76, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 79, 'end': 81, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 84, 'end': 86, 'color': 'rgb(0,0,200)',
     'bgcolor': 'rgb(200,200,0)', 'tooltip': 'Turn', 'underscore': False},
    {'start': 91, 'end': 97, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 98, 'end': 101, 'color': 'rgb(255,255,255)',
     'bgcolor': 'rgb(0,0,255)', 'tooltip': 'Beta strand', 'underscore': True},
    {'start': 102, 'end': 106, 'color': 'rgb(0,0,0)',
     'bgcolor': 'rgb(100,100,200)', 'tooltip': 'Helix', 'underscore': True},
    {'start': 107, 'end': 109, 'color': 'rgb(0,0,200)',
     'bgcolor': 'rgb(200,200,0)', 'tooltip': 'Turn', 'underscore': False}
]


def header_colors():
    return {
        'bg_color': '#fff',
        'font_color': 'black'
    }


def description():
    return 'Display protein and nucleotide sequences with \
    coverages, selection information, and search.'


def layout():

    return html.Div(id='seq-view-body', className='app-body', children=[
        html.Div(
            id='seq-view-container',
            children=[
				
            ]
        ),
        html.Div(id='seq-view-control-tabs', className='control-tabs', children=[
			html.Div(
            id='seq-view-containertest',
            children=[
            dcc.Tabs(id='seq-view-tabs', value='what-is', children=[
                dcc.Tab(
                    label='About',
                    value='what-is',
                    children=html.Div(className='control-tab', children=[
                        html.H4(className='what-is', children='Resume Match'),
                        html.P('Upload your resumes and select job roles to find the ideal match %'),
                        html.P('You can additionally add job descriptions to customize your resume'),
                    ])
                ),
                dcc.Tab(
                    label='Best Fit Role',
                    value='data1',
                    children=html.Div(className='control-tab', children=[
                        html.Div(
                            id='seq-view-fasta-upload1',
                            children=[
                             "Step 1: Upload Resume File ",html.Br(),
                                dcc.Upload(
                                    id='upload-fasta-data1',
                                    className='control-upload',
                                    children=html.Div(id='upload-filename1',children=[ 
                                        "Drag and drop or click to upload a \
                                        Resume File."
                                    ]),
                                ),
                            ]
                        ),
                        html.Br(),
                        dbc.Button('Submit', id='submit-val1', n_clicks=0),
                ])),
                dcc.Tab(
                    label='Analyze Resume',
                    value='data',
                    children=html.Div(className='control-tab', children=[
						html.Div(
                            id='seq-view-fasta-upload',
                            children=[
                            "Step 1: Upload Resume File ",
							 dcc.Upload(
                                    id='upload-fasta-data',
                                    className='control-upload',
                                    children=[html.Div(id='upload-filename',children=[ 
                                        "Click to upload a \
                                        Resume File."]
                                    )],
                                ),
                            
                            ]	
                        ),
						
                        html.Div(className='app-controls-block', children=[
							html.Div(
							html.Label(["Step 2: Select a Job Role ",html.Br(),
                            dcc.Dropdown(
                                className='app-dropdown',
                                id='preloaded-sequences',
                                options=[
                                    {
                                        'label': 'Data Scientist',
										'value': 'Data Scientist'
                                    },
                                    {
                                        'label': 'Machine Learning Engineer',
										'value': 'Machine Learning Engineer',
                                    }
                                ],placeholder="Select a Job Role",value='Data Scientist'
                             
                            )
							]))
                        ]),
						html.Div(   className='app-controls-block', children=[
                            html.Div(
                                html.Label(["Step 3: Enter Job Description(Optional) ",html.Br(),
                            dcc.Textarea(
                                className='app-dropdown',
                                id='preloaded-sequences-textarea',
								value='Please Copy-Paste Job Description Here',
								style={'width': '90%', 'height': 200},
                            ),
							html.Div(
                                    id='textarea-example-output',
                                    className='app-controls-name'
                                )
                        ])
						)
						]),
                        dbc.Button('Submit', id='submit-val', n_clicks=0),
                     
                    ])
                ),
				dcc.Tab(
                    label='Resume Comparison',
                    value='Resume Comparison',
                    children=html.Div(className='control-tab', children=[
						html.Div(
                            id='seq-view-upload2',
                            children=[
                            "Step 1: Upload Multiple Resume Files ",
							 dcc.Upload(
                                    id='upload-data2',
                                    className='control-upload',
                                    children=[html.Div(id='upload-filename2',children=[ 
                                        "Click to upload a \
                                        Resume File."]
                                    )],multiple=True,
                                ),
                            
                            ]	
                        ),
                        html.Div(className='app-controls-block', children=[
							html.Div(children=
							["Step 2: Select a Job Role ",html.Br(),
                            dcc.Dropdown(
                                className='app-dropdown',
                                id='preloaded-sequences2',
                                options=[
                                    {
                                        'label': 'Data Scientist',
										'value': 'Data Scientist'
                                    },
                                    {
                                        'label': 'Machine Learning Engineer',
										'value': 'Machine Learning Engineer',
                                    }
                                ],placeholder="Select a Job Role",value='Data Scientist'
                             
                            )
							])
                        ]),
						html.Div(className='app-controls-block', children=[
                            html.Div(children=
                                ["Step 3: Enter Job Description(Optional) ",html.Br(),
                            dcc.Textarea(
                                className='app-dropdown',
                                id='preloaded-sequences-textarea2',
								value='Please Copy-Paste Job Description Here',
								style={'width': '90%', 'height': 200},
                            ),
                        ]
						)
						]),						
						
                        
                        dbc.Button('Submit', id='submit-val2', n_clicks=0),
                     
                    ])
                )
            ]),
            
			]),
			
        ]),

    ])



def callbacks(_app):

    @_app.callback(Output('seq-view-container', 'children'),
              [Input('seq-view-tabs', 'value')])
    def render_content(tab):
        if tab == 'data1':
            return html.Div(
            id='seq-view-data1',
            children=[
			    html.Div(id='text-info-desc',
                                 children=[
                                     html.Span(
                                         "Best Fit ",
                                         className='seq-view-info-element-title'
                                     ),
                                     html.Div(
                                         id='text-info',
                                         children=[]
                                     )
                                 ]),
								 
                html.Div(id='seq-view-info-container', children=html.Div(
                    id='seq-view-info',
                    children=[
                        html.Div(id='seq-view-info-desc2',
                                 children=[
                                     html.Span(
                                         "Best Fit Comparision",
                                         className='seq-view-info-element-title'
                                     ),
                                     html.Div(
                                         id='desc-info2',
                                         children=[
										 dcc.Graph(
                            id='bar-graph',
                            figure={
                              'data': [{'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF','marker': {'color': ['#32CD32', '#7FFF00', '#7CFC00','#00FF00']}}],
                              
                            }
                        )]
                                     ),
                                     
                                 ]),
							 ]
                )),
				html.Div(id='text-info-desc1',
                                 children=[
                                     html.Span(
                                         "Word Cloud",
                                         className='seq-view-info-element-title'
                                     ),
                                     html.Div(
                                         id='text-info1',
                                         children=[]
                                     ),
									 html.Div([
                                           html.Img(id="image_wc"),
                                      ]),
									 html.Div(id='hidden-div', children=[])
                                 ]),

            ]
        )
								 
            
        elif tab == 'data':
            return html.Div(
            id='seq-view-data2',
            children=[             
			      html.Div(id='seq-view-donut',children=[
                         html.Div(
                             className="row",id='seq-view-donut1',
                             children=[
                                 html.Div(
                                     className="six columns",
                                     children=[
                                         html.Div(
                                             children=dcc.Graph(id='left-top-bar-graph',
                                             figure={
								'data': [
									{'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
									{'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
								]
							}

											 )
                                         )
                                     ]
                                 ),
                                 html.Div(
                                     className="six columns",
                                     children=html.Div(
                                         children=dcc.Graph(id='right-top-pie-graph',figure=fig
										 ),
                                     )
                                 )
                             ]
                         ),
                     ]),
                     html.Div(
                     id='seq-view-info1',
                     children=[
                         
                         html.Div(id='seq-view-info-desc',
                                  children=[
                                      html.Span(
                                          "Description",
                                          className='seq-view-info-element-title'
                                      ),
                                      html.Div(
                                          id='desc-info',
                                          children=[dash_table.DataTable(
                                          id='table-info',
                                          columns = [
                                             {"id": 'skills', "name": "skills"},
                                             {"id": 'count', "name": "count"}],
                                          data=[],),dbc.Button(children ="", id="example-button", className="mr-2")
                                         
                                          ]
                                      ),
                                      html.Div(
                                          id='jd-info',
                                          children=[dash_table.DataTable(
                                          id='table-jd-info',
                                          columns = [
                                             {"id": 'skills', "name": "skills"},
                                             {"id": 'count', "name": "count"}],
                                          data=[],),dbc.Button(children ="", id="example2-button", className="mr-2")
                                         
                                          ]
                                      )
                                  ]),
					 	html.Div(id='text-info-desc2',
                                  children=[
                                      html.Span(
                                          "Job Description Info",
                                          className='seq-view-info-element-title1'
                                      ),
                                      html.Div(
                                          id='text-info2',
                                          children=[]
                                      ),
									  
                                  ]),	 

                         html.Div(id='hidden-div', children=[
						 html.Span(className='hidden'),]),
                     ]
                 )])
				 
        elif tab == 'Resume Comparison':
            return html.Div(
            id='seq-view-data2',
            children=[             
			      html.Div(id='seq-view-donut',children=[
                         html.Div(
                             className="row",id='seq-view-donut1',
                             children=[
                                 html.Div(
                                     className="six columns",
                                     children=[
                                         html.Div(
                                             children=dcc.Graph(id='left-top-bar-graph',
                                             figure={
								'data': [
									{'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
									{'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
								]
							}

											 )
                                         )
                                     ]
                                 ),
                                 html.Div(
                                     className="six columns",
                                     children=html.Div(
                                         children=dcc.Graph(id='right-top-pie-graph',figure=fig
										 ),
                                     )
                                 )
                             ]
                         ),
                     ]),
                     html.Div(
                     id='seq-view-info4',
                     children=[
                         
                         html.Div(id='seq-view-info-desc',
                                  children=[
                                      html.Span(
                                          "Description",
                                          className='seq-view-info-element-title'
                                      ),
                                      html.Div(
                                          id='desc-info2',
                                          children=[dash_table.DataTable(
                                          id='table-info2',
                                          columns = [
                                             {"id": 'filename', "name": "filename"},
                                             {"id": 'Percent_match', "name": "Percent_match"}],
                                          data=[],),dbc.Button(children ="", id="example-button2", className="mr-2")
                                         
                                          ]
                                      ),
                                      html.Div(
                                          id='jd-info2',
                                          children=[dash_table.DataTable(
                                          id='table-jd-info2',
                                          columns = [
                                             {"id": 'filename', "name": "filename"},
                                             {"id": 'Percent_match', "name": "Percent_match"}],
                                          data=[],),dbc.Button(children ="", id="example2-button2", className="mr-2")
                                         
                                          ]
                                      )
                                  ]),
					 	html.Div(id='text-info-desc2',
                                  children=[
                                      html.Span(
                                          "Job Description Info",
                                          className='seq-view-info-element-title1'
                                      ),
                                      html.Div(
                                          id='text-info2',
                                          children=[]
                                      ),
									  
                                  ]),	 

                         html.Div(id='hidden-div2', children=[
						 html.Span(className='hidden'),]),
                     ]
                 )])

        


# upload or preloaded
    @_app.callback(
        Output('upload-filename', 'children'),
        [Input('upload-fasta-data', 'filename')]
    )
    def update_filename(filename):
        return str(filename)
		
    @_app.callback(
        Output('upload-filename1', 'children'),
        [Input('upload-fasta-data1', 'filename')]
    )
    def update_filename1(filename):
        return str(filename)

    @_app.callback(
        Output('upload-filename2', 'children'),
        [Input('upload-data2', 'filename')]
    )
    def update_filename2(filename):
        return str(filename)		
  
		
  
    @_app.callback(
        Output('text-info2', 'children'),
        [Input('preloaded-sequences-textarea', 'value')]
    )
    def update_output_text(value):
        return 'You have entered: \n{}'.format(value)
	

    
    def parse_contents(contents, filename, date):
       content_type, content_string = contents.split(',')
       decoded = base64.b64decode(content_string)
      
       return decoded.decode('utf-8')

    # @_app.callback([Output('table-jd-info', 'data'),Output('example2-button', 'children')],
    #           [Input('submit-val', 'n_clicks')],
    #           [State('preloaded-sequences-textarea', 'value'),
    #           State('upload-fasta-data', 'contents'),
    #             State('upload-fasta-data', 'filename'),
    #             State('upload-fasta-data', 'last_modified'),
    #             State('preloaded-sequences', 'value')]
    #           )
    
    # def display_jd_comparisom(nclicks,val):
    #     print(len(val))
    #     if val == 'Please Copy-Paste Job Description Here':
    #         return pd.DataFrame({}).to_dict(orient='records'),0
    #     elif len(val) < 150:
    #         return pd.DataFrame({}).to_dict(orient='records'),0
    #     else:
    #         match_df,match_percent = NLP.jd_handler(val)
    #         match_df=match_df.reset_index().rename(columns={'index':'skills'})
    #         return match_df.to_dict(orient='records'),match_percent




    # def update_output(uploaded_filenames, uploaded_file_contents):
    #         """Save uploaded files and regenerate the file list."""

    #         if uploaded_filenames is not None and uploaded_file_contents is not None:
    #                for name, data in zip(uploaded_filenames, uploaded_file_contents):
    #                     save_file(name, data)
    #         print(uploaded_filenames)

    #         files = uploaded_files()
    #         if len(files) == 0:
    #             return [html.Li("No files yet!")]
    #         else:
    #             return [html.Li(file_download_link(filename)) for filename in files]
    


		
	

    UPLOAD_DIRECTORY = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'upload_dir')


    if not os.path.exists(UPLOAD_DIRECTORY):
        os.makedirs(UPLOAD_DIRECTORY)
		
    def download(path):
        """Serve a file from the upload directory."""
        return send_from_directory(UPLOAD_DIRECTORY, path, as_attachment=True)
	
    def save_file(name, content):
        """Decode and store a file uploaded with Plotly Dash."""
        # print("The content is"+content+"name is "+name)
        data = content.encode("utf8").split(b";base64,")[1]
        #print(data)
        with open(os.path.join(UPLOAD_DIRECTORY, name), "wb") as fp:
            fp.write(base64.decodebytes(data))


    def uploaded_files():
        """List the files in the upload directory."""
        files = []
        for filename in os.listdir(UPLOAD_DIRECTORY):
            path = os.path.join(UPLOAD_DIRECTORY, filename)
            if os.path.isfile(path):
                files.append(filename)
        return files


    def file_download_link(filename):
        """Create a Plotly Dash 'A' element that downloads a file from the app."""
        location = "/download/{}".format(urlquote(filename))
        return html.A(filename, href=location)


    def plot_wordcloud(data):
        d = {a: x for a, x in data.values}
        wc = WordCloud(background_color='white', width=580, height=300)
        wc.fit_words(d)
        return wc.to_image()

    
#    @_app.callback(dd.Output('image_wc', 'src'), [dd.Input('image_wc', 'id')])
#    def make_image(b):
#        img = BytesIO()
#        plot_wordcloud(data=dfm).save(img, format='PNG')
#        return 'data:image/png;base64,{}'.format(base64.b64encode(img.getvalue()).decode())

    @_app.callback([Output('text-info', 'children'),
                    Output('bar-graph', 'figure'),
					dd.Output('image_wc', 'src')],
              [Input('submit-val1', 'n_clicks'),
			    dd.Input('image_wc', 'id')],
              [State('upload-fasta-data1', 'contents'),
                State('upload-fasta-data1', 'filename'),
                State('upload-fasta-data1', 'last_modified')])
    def update_output1(nclicks,image_wc,list_of_contents, list_of_names, list_of_dates):
        # print("Hello")
        if list_of_contents is not None and list_of_names is not None:
            save_file(list_of_names,list_of_contents,)
            filepath = UPLOAD_DIRECTORY + "/" + list_of_names
            final_count_match, resume_counts = NLP.best_fit(filepath)
            print(final_count_match)
            Keymax = max(final_count_match, key=final_count_match.get)
            sort_orders = {k: v for k, v in sorted(resume_counts.items(), key=lambda item: item[1], reverse=True)}
            print(sort_orders)
            print(Keymax)
            words = list(sort_orders.keys())[:30]
            frequency= list(sort_orders.values())[:30]
            dfm = pd.DataFrame({'word': words, 'freq': frequency})
            img2 = BytesIO()
            plot_wordcloud(data=dfm).save(img2, format='PNG')
           
            headtxt = "The best role for the resume is: " + Keymax
            data = {'data':[{'x': list(final_count_match.keys()), 'y': list(final_count_match.values()), 'type': 'bar', 'name': 'SF','marker': {'color': ['#32CD32', '#7FFF00', '#7CFC00','#00FF00']}}]}
            print(data)
            dfm2=pd.DataFrame({'word': ['python','excel','machine_learning','tableau','sql'], 'freq': [1,3,9,10,11]})
            img1 = BytesIO()
            plot_wordcloud(data=dfm2).save(img1, format='PNG')
            return headtxt,data,'data:image/png;base64,{}'.format(base64.b64encode(img2.getvalue()).decode())
        else: 
            return 'Please Upload the Resume',{
                              'data': [{'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF','marker': {'color': ['#32CD32', '#7FFF00', '#7CFC00','#00FF00']}}]
                            },'Please Upload File First'
		
    @_app.callback([Output('table-info', 'data'),
                    Output('example-button', 'children'),
                    Output("hidden-div", "children"),
                    Output('table-jd-info', 'data'),
                    Output('example2-button', 'children')],
              [Input('submit-val', 'n_clicks')],
              [State('upload-fasta-data', 'contents'),
                State('upload-fasta-data', 'filename'),
                State('upload-fasta-data', 'last_modified'),
                State('preloaded-sequences', 'value'),
                State('preloaded-sequences-textarea','value')]
            )
    def update_output(nclicks,list_of_contents, list_of_names, list_of_dates,val1,val2):
        x=[html.Li("No files yet!")]
        match_df,match_percent = pd.DataFrame({}).to_dict(orient='records'),0
        # print(list_of_contents)
        # print(list_of_names)
        if list_of_contents is not None and list_of_names is not None:
            # for name, data in zip(list_of_contents, list_of_names):
                # print(name)
                # print(data)
            save_file(list_of_names,list_of_contents)
            #print(list_of_contents)

            files = uploaded_files()
            if len(files) == 0:
                x= [html.Li("No files yet!")]
            else:
                x= [html.Li(file_download_link(filename)) for filename in files]
            children = NLP.get_match_percent(val1,'jobrole')
            print(type(children[0]))
            print(children[0])
            if val2 == 'Please Copy-Paste Job Description Here':
                match_df,match_percent = pd.DataFrame({}),0
            elif len(val2) < 2:
                match_df,match_percent = pd.DataFrame({}),0
            else:
                match_df,match_percent = NLP.get_match_percent(str(val2),'jd')
                match_df=match_df.reset_index().rename(columns={'index':'skills'})
            print("match_df is")
            print(match_df)        
            a=pd.DataFrame.from_dict(children[0],orient='index',columns=['count'])
            b=pd.DataFrame.from_dict(children[1],orient='index',columns=['count'])
            a=a.append(b)
            a=a.reset_index().rename(columns={'index':'skills'})
            print(a)
            return a.to_dict(orient='records'),str(round(children[2],2))+"%",x,match_df.to_dict(orient='records'),match_percent

        else:
            return pd.DataFrame({}).to_dict(orient='records'),"noFilesyet",x,pd.DataFrame({}).to_dict(orient='records'),0
		
    @_app.callback([Output('table-info2', 'data'),
                    Output('example-button2', 'children'),
                    Output("hidden-div2", "children"),
                    Output('table-jd-info2', 'data'),
                    Output('example2-button2', 'children'),],
              [Input('submit-val2', 'n_clicks')],
              [State('upload-data2', 'contents'),
                State('upload-data2', 'filename'),
                State('upload-data2', 'last_modified'),
                State('preloaded-sequences2', 'value'),
                State('preloaded-sequences-textarea2','value')]
            )
    def update_output2(nclicks,list_of_contents, list_of_names, list_of_dates,val1,val2):
        x=[html.Li("No files yet!")]
        print("control is here")
        match_df,match_percent = pd.DataFrame({}),0
        bestfit2="No jd Provided"
        if list_of_contents is not None and list_of_names is not None:
            for name, data in zip(list_of_names,list_of_contents):
                save_file(name, data)
            #print(list_of_contents)

            files = uploaded_files()
            if len(files) == 0:
                x= [html.Li("No files yet!")]
            else:
                x= [html.Li(file_download_link(filename)) for filename in files]
            resume_df = NLP.resume_lister(val1,key='jobRole')
            bestfit=str(resume_df['filename'].head(1))
            if val2 == 'Please Copy-Paste Job Description Here':
               match_df,match_percent = pd.DataFrame({}),0
            elif len(val2) < 150:
               match_df,match_percent = pd.DataFrame({}),0
            else:
               match_df = NLP.resume_lister(str(val2),key='jd')
               bestfit2=str(resume_df['filename'].head(1))
            print(NLP.del_uploaded_files()) 
            print("resume df is")
            print(resume_df)

            return [resume_df.to_dict(orient='records'),bestfit,x,match_df.to_dict(orient='records'),bestfit2]
        else:
            return pd.DataFrame({}).to_dict(orient='records'),"noFilesyet",x,pd.DataFrame({}).to_dict(orient='records'),0
            #return pd.DataFrame({}).to_dict(orient='records'),"noFilesyet",x,pd.DataFrame({}).to_dict(orient='records'),0	

# only declare app/server if the file is being run directly
if 'DEMO_STANDALONE' not in os.environ:
    app = run_standalone_app(layout, callbacks, header_colors, __file__)
    server = app.server

if __name__ == '__main__':
    app.run_server(debug=True, port=8050)
    #app.run_server(,host='10.10.110.197')

